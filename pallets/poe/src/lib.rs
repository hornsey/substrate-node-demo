#![cfg_attr(not(feature = "std"), no_std)]  

pub use pallet::*;

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;


#[frame_support::pallet]
pub mod pallet {
    use frame_support::pallet_prelude::*;
    use frame_system::pallet_prelude::*;


    #[pallet::config]
    pub trait Config: frame_system::Config {
        #[pallet::constant]
        type MaxClaimLength: Get<u32>;
        type RuntimeEvent: From<Event<Self>> + IsType<<Self as frame_system::Config>::RuntimeEvent>;
    }


    #[pallet::pallet]
    pub struct Pallet<T>(_);

    #[pallet::storage]
    #[pallet::getter(fn proofs)]
    pub type Proofs<T: Config> = StorageMap<
        _, 
        Blake2_128Concat, 
        BoundedVec<u8, T::MaxClaimLength>, 
        (T::AccountId, T::BlockNumber)
        >;

    #[pallet::event]
    #[pallet::generate_deposit(pub(super) fn deposit_event)]
    pub enum Event<T: Config> {
        ClaimeCreated(T::AccountId, BoundedVec<u8, T::MaxClaimLength>),
        ClaimeRevoked(T::AccountId, BoundedVec<u8, T::MaxClaimLength>),
        ClaimTransfered(T::AccountId, T::AccountId, BoundedVec<u8, T::MaxClaimLength>),
    }

    #[pallet::error]
    pub enum Error<T> {
        ProofAlreadyExists,
        ClaimNotExist,
        ClaimTooLong,
        NotClaimOwner
    }

    #[pallet::hooks]
    impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {}


    #[pallet::call]
    impl<T: Config> Pallet<T> {
        #[pallet::weight(0)]
        #[pallet::call_index(0)]
        pub fn create_claim(
            origin: OriginFor<T>,
            claim: BoundedVec<u8, T::MaxClaimLength>
        ) -> DispatchResultWithPostInfo {
            let sender = ensure_signed(origin)?;
            // ensure!(claim.len() <= T::MaxClaimLength::get(), Error::<T>::ClaimTooLong);
            ensure!(!Proofs::<T>::contains_key(&claim), Error::<T>::ProofAlreadyExists);
            Proofs::<T>::insert(&claim, (sender.clone(), <frame_system::Pallet<T>>::block_number()));
            Self::deposit_event(Event::ClaimeCreated(sender, claim));
            Ok(().into())
        }


        #[pallet::weight(0)]
        #[pallet::call_index(1)]    
        pub fn revoke_claim(
            origin: OriginFor<T>,
            claim: BoundedVec<u8, T::MaxClaimLength>
        ) -> DispatchResultWithPostInfo {
            let sender = ensure_signed(origin)?;

            ensure!(Proofs::<T>::contains_key(&claim), Error::<T>::ClaimNotExist);
            let (owner, _) = Proofs::<T>::get(&claim).ok_or(Error::<T>::ClaimNotExist)?;
            ensure!(owner == sender, Error::<T>::NotClaimOwner);

            Proofs::<T>::remove(&claim);

            Self::deposit_event(Event::ClaimeRevoked(sender, claim));
            Ok(().into())
        }

        #[pallet::weight(0)]
        #[pallet::call_index(2)]
        pub fn transfer_claim(
            origin: OriginFor<T>,
            claim: BoundedVec<u8, T::MaxClaimLength>,
            dest: T::AccountId,
            // dest: <T::Lookup as StaticLookup>::Source
        )
            -> DispatchResultWithPostInfo {
            let sender = ensure_signed(origin)?;

            ensure!(Proofs::<T>::contains_key(&claim), Error::<T>::ClaimNotExist);
            let (owner, _) = Proofs::<T>::get(&claim).ok_or(Error::<T>::ClaimNotExist)?;
            ensure!(owner == sender, Error::<T>::NotClaimOwner);

            // let dest = T::Lookup::lookup(dest)?;
            Proofs::<T>::insert(&claim, (dest.clone(), <frame_system::Pallet<T>>::block_number()));
            Self::deposit_event(Event::ClaimTransfered(sender, dest, claim));
            Ok(().into())
        }

    }
    

}